import { writable, readable } from 'svelte/store';
import tailwindTheme from 'tailwindcss/defaultTheme';

export const colors = writable(tailwindTheme.colors);
