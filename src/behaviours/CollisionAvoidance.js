import Vector2D from '../data/Vector2D.js';
export default function (transform, room) {
    [
        {x: -1, y: -1},
        {x: -1, y: 0},
        {x: -1, y: 1},
        {x: 0, y: -1},
        {x: 0, y: 0},
        {x: 0, y: 1},
        {x: 1, y: -1},
        {x: 1, y: 0},
        {x: 1, y: 1},
    ]
    .map(position => {
        // sorounding spaces
        return new Vector2D(Math.floor(transform.position.x) + position.x, Math.floor(transform.position.y) + position.y)
    })
    .filter(position => room.spaceBlocked(position.x, position.y)) // only blocking spaces
    .map(position => {
        // vector to potential collision point
        return new Vector2D(
            Math.clamp(transform.position.x, position.x, position.x + 1),
            Math.clamp(transform.position.y, position.y, position.y + 1)
        );
    }).forEach(collisionPoint => {
        let toCollisionPoint = collisionPoint.subtract(transform.position);
        if (toCollisionPoint.magnitudeSquared <= 0.0625) {
            transform.position = collisionPoint.subtract(toCollisionPoint.withMagnitude(0.25));
        }
    });
}
