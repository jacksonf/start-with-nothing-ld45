export default class Room {

    constructor () {
        this.grid = getNoise().map(space => space > .68 ? true : false);
    }

    spaceBlocked (x, y) {
        return this.grid[y * 16 + x];
    }
}
