import Transform from './Transform.js';
import Vector2D from './Vector2D.js';
import collisionAvoidance from '../behaviours/CollisionAvoidance.js';

export default class Player {

    constructor () {
        this.transform = new Transform;
        this.keys = new Set([]);
    }

    update (delta) {
        this.transform.update(delta);
        return this;
    }

    fixedUpdate (room) {
        let desiredVelocity = new Vector2D;
		if (this.keys.has('a') && !this.keys.has('d')) {
			desiredVelocity.x = -1;
		} else if (this.keys.has('d') && !this.keys.has('a')) {
			desiredVelocity.x = 1;
		}
		if (this.keys.has('w') && !this.keys.has('s')) {
			desiredVelocity.y = -1;
		} else if (this.keys.has('s') && !this.keys.has('w')) {
			desiredVelocity.y = 1;
        }
        desiredVelocity = desiredVelocity.withMagnitude(0.08);
        collisionAvoidance(this.transform, room);
        this.transform.addForce(desiredVelocity.subtract(this.transform.velocity).limit(.01));
        this.transform.fixedUpdate();
    }
}
