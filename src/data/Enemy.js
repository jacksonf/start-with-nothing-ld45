import Transform from './Transform.js';
import Vector2D from './Vector2D.js';
import collisionAvoidance from '../behaviours/CollisionAvoidance.js';

export default class Enemy {

    constructor (target, position, room) {
        this.transform = new Transform(position);
        this.target = target;
        this.seeking = false;
        this.targetInRange = false;
        this.room = room;
        this.layout = getNoise().map((value, index) => {
            let angle = value * 2 * Math.PI;
            let direction = Vector2D.create( Math.cos(angle), Math.sin(angle) ).unit();
            let x = index % 16;
            let y = Math.floor(index / 16);

            // point direction away from obstacles

            // top left
            if (x > 0 && y > 0 && room.spaceBlocked(x - 1, y - 1) && direction.x < 0 && direction.y < 0) {
                direction = direction.add(new Vector2D(1, 1)).unit();
            }

            // top
            if (y > 0 && room.spaceBlocked(x, y - 1)) {
                direction.y = Math.abs(direction.y);
            }

            // top right
            if (x < 15 && y > 0 && room.spaceBlocked(x + 1, y - 1) && direction.x > 0 && direction.y < 0) {
                direction = direction.add(new Vector2D(-1, 1)).unit();
            }

            // right
            if (x < 15 && room.spaceBlocked(x + 1, y)) {
                direction.x = Math.abs(direction.x) * -1;
            }

            // bottom right
            if (x < 15 && y < 15 && room.spaceBlocked(x + 1, y + 1) && direction.x > 0 && direction.y > 0) {
                direction = direction.add(new Vector2D(-1, -1)).unit();
            }

            // bottom
            if (y < 15 && room.spaceBlocked(x, y + 1)) {
                direction.y = Math.abs(direction.y) * -1;
            }

            // bottom left
            if (x > 0 && y < 15 && room.spaceBlocked(x - 1, y + 1) && direction.x < 0 && direction.y > 0) {
                direction = direction.add(new Vector2D(1, -1)).unit();
            }

            // left
            if (x > 0 && room.spaceBlocked(x - 1, y)) {
                direction.x = Math.abs(direction.x);
            }
            return direction;
        });
        console.log(this.layout);
    }

    update (delta) {
        this.transform.update(delta);
        return this;
    }

    fixedUpdate (enemies) {
        collisionAvoidance(this.transform, this.room);
        let toTarget = this.target.position.subtract(this.transform.position);
        this.targetInRange = toTarget.magnitude < 3 && Math.abs(toTarget.degrees - this.transform.velocity.degrees) <= 45;
        this.seeking = this.seeking ? true : this.targetInRange;
        if (false && this.seeking) {
            let closest = !enemies.filter(enemy => enemy.seeking).some(enemy => {
                return enemy.transform.position.subtract(this.target.position).magnitude
                    < this.transform.position.subtract(this.target.position).magnitude;
            });
            if (closest) {
                let scale = this.target.position.subtract(this.transform.position).magnitude / 3;
                this.transform.addForce(
                    this.target.position.subtract(this.transform.position)
                    .withMagnitude(0.05 * scale)
                    .subtract(this.transform.velocity)
                    .limit(.001)
                )
            } else {
                let seperation = enemies.map(enemy => enemy.transform).filter(enemyTransform => {
                    let distance = enemyTransform.position.subtract(this.transform.position).magnitude;
                    return distance > 0 && distance < 3;
                }).reduce((average, enemyTransform, index, filtered) => {
                    let toEnemy = enemyTransform.position.subtract(this.transform.position);
                    return average.add(toEnemy.withMagnitude(-1 / filtered.length));
                }, new Vector2D);
                this.transform.addForce(
                    seperation
                    .unit()
                    .add(this.target.position.add(this.target.velocity.multiply(60)).subtract(this.transform.position).unit())
                    .withMagnitude(0.05)
                    .subtract(this.transform.velocity)
                    .limit(.001)
                );
            }
        } else {
            let seperation = enemies.map(enemy => enemy.transform).filter(enemyTransform => {
                let distance = enemyTransform.position.subtract(this.transform.position).magnitude;
                return distance > 0 && distance < 3;
            }).reduce((average, enemyTransform, index, filtered) => {
                let toEnemy = enemyTransform.position.subtract(this.transform.position);
                return average.add(toEnemy.withMagnitude(-1 / filtered.length));
            }, new Vector2D);
            this.transform.addForce(
                seperation
                .unit()
                .add(this.layout[Math.floor(this.transform.position.y) * 16 + (Math.floor(this.transform.position.x))])
                .withMagnitude(0.05)
                .subtract(this.transform.velocity)
                .limit(.001)
            );
        }
        this.transform.fixedUpdate();
    }
}
