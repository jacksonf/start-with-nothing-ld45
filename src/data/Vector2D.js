export default class Vector2D {

    constructor (x = 0, y = 0) {
        this.x = x;
        this.y = y;
    }

    get magnitude () {
        return Math.sqrt(this.x * this.x + this.y * this.y);
    }

    get magnitudeSquared () {
        return this.x * this.x + this.y * this.y;
    }

    get degrees () {
        if (this.x === 0) {
            return this.y > 0 ? 180 : 0;
        }
        return Math.atan (this.y / this.x) * 180 / Math.PI + (this.x > 0 ? 90 : -90);
    }

    get copy () {
        return new Vector2D(this.x, this.y);
    }

    static create (x, y) {
        return new Vector2D (x, y);
    }

    add (otherVector) {
        return new Vector2D (this.x + otherVector.x, this.y + otherVector.y);
    }

    subtract (otherVector) {
        return new Vector2D (this.x - otherVector.x, this.y - otherVector.y);
    }

    unit () {
        if (this.magnitude === 0) {
            return new Vector2D (0, 0);
        }
        return new Vector2D (this.x / this.magnitude, this.y / this.magnitude);
    }

    multiply (scalar) {
        return new Vector2D (this.x * scalar, this.y *scalar);
    }

    withMagnitude (magnitude) {
        if (this.magnitude === 0) {
            return new Vector2D;
        }
        return new Vector2D (this.x / this.magnitude * magnitude, this.y / this.magnitude * magnitude);
    }

    limit (limit) {
        return this.magnitude <= limit ? this : this.withMagnitude(limit);
    }

    projectOnto (other) {
        let scalarProjection = (this.x * other.x + this.y + other.y ) / other.magnitude;
        return other.withMagnitude(scalarProjection);
    }

}
