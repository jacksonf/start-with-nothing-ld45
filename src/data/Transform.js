import Vector2D from './Vector2D.js';

export default class Transform {

    constructor (
        position = new Vector2D,
        velocity = new Vector2D,
        acceleration = new Vector2D,
    ) {
        this.position = position;
        this.velocity = velocity;
        this.acceleration = acceleration;
        this.collisionCorrection = new Vector2D(0, 0);
    }

    update (delta) {
        this.position = this.position.add(this.velocity.multiply(delta)).add(this.collisionCorrection);
        this.collisionCorrection = new Vector2D(0 ,0);
    }

    fixedUpdate () {
        this.velocity = this.velocity.add(this.acceleration);
        this.acceleration = new Vector2D;
    }

    addForce (force) {
        this.acceleration = this.acceleration.add(force);
    }

}
