import App from './components/App.svelte';
import styles from './main.css';
import Noise from 'open-simplex-noise';

window.noise = new Noise(Date.now());
window.getNoise = () => {
	let points = [];
	let scale = .2;
	let random = Math.random() * 10000;
	for (let y = 0; y < 9; y++) {
		for (let x = 0; x < 16; x++) {
			points[16 * y + x] = (noise.noise3D(x / scale / 16, y / scale / 9, random) + 1)  * 0.5;
		}
	}
	return points;
}

Math.clamp = (number, min, max) => {
	if (number < min) {
		return min;
	}
	if (number > max) {
		return max;
	}
	return number;
}

const app = new App({
	target: document.body
});

export default app;
